module.exports = {
  root: true,
  env: {
    node: true,
    'jest/globals': true,
  },
  rules: {
    'indent': ['error', 2],
    'quotes': ['error', 'single', { 'avoidEscape': true }],
    'react/require-extension': 'off',
    'react/jsx-filename-extension': [4, { extensions: ['.js', '.jsx', '.ts', '.tsx'] }],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'semi': [1, 'never']
  },
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    'eslint-plugin-jest'
  ],
  extends: [
    'airbnb-base',
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended'
  ],
  settings: {
    'import/parsers': {
      '@typescript-eslint/parser': ['.ts', '.tsx'],
    },
    'import/resolver': {
      typescript: {},
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    },
  },
}

