/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable import/prefer-default-export */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Component, Prop, h } from '@stencil/core'

@Component({
  tag: 'error-message',
  styleUrl: 'error.css'
})
export class Error {
  @Prop() message: string

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  render() {
    return (
      <div class='error'><p>{this.message}</p></div>
    )
  }
}