# error-message



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute | Description | Type     | Default     |
| --------- | --------- | ----------- | -------- | ----------- |
| `message` | `message` |             | `string` | `undefined` |


## Dependencies

### Used by

 - [pdf-viewer](../viewer)

### Graph
```mermaid
graph TD;
  pdf-viewer --> error-message
  style error-message fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
