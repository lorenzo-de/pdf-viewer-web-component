# pdf-viewer



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type     | Default |
| ------------- | -------------- | ----------- | -------- | ------- |
| `canvasWidth` | `canvas-width` |             | `number` | `500`   |
| `url`         | `url`          |             | `string` | `''`    |


## Dependencies

### Depends on

- [error-message](../error)
- [pdf-toolbar](../toolbar)

### Graph
```mermaid
graph TD;
  pdf-viewer --> error-message
  pdf-viewer --> pdf-toolbar
  style pdf-viewer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
