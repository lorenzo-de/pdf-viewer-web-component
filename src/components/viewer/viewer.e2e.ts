// eslint-disable-next-line react/jsx-filename-extension
import { newE2EPage } from '@stencil/core/testing'

describe('pdf-viewer', () => {
  it('renders', async () => {
    const page = await newE2EPage()
    await page.setContent(
      '<pdf-viewer url="http://example.org/test.pdf"></pdf-viewer>'
    )

    const e = await page.find('pdf-viewer')
    expect(e).toHaveClass('hydrated')
  })

  it('has a canvas element with id "tt-pdf-widget"', async () => {
    const page = await newE2EPage()
    await page.setContent(
      '<pdf-viewer url="http://example.org/test.pdf"></pdf-viewer>'
    )
    const e = await page.find('canvas#tt-pdf-widget')
    expect(e).not.toBeNull()
  })

  // it('displays error message when PDF URL is not a valid URL', async () => {
  //   const page = await newE2EPage()
  //   try {
  //     await page.setContent(`<pdf-viewer url="not a url"></pdf-viewer>`)
  //   } catch(e) {
  //     console.log(e)
  //     expect(e).not.toBeNull()
  //   }
  // })
})
