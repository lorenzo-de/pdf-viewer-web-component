/* eslint-disable @typescript-eslint/no-inferrable-types */
/* eslint-disable import/prefer-default-export */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Component, Element, Prop, Listen, State, h} from '@stencil/core'
import pdfjsLib from 'pdfjs-dist'
import pdfjsWorker from 'pdfjs-dist/build/pdf.worker.entry'

import { isValidUrl } from '../../utils/utils'

@Component({
  tag: 'pdf-viewer',
  styleUrl: 'viewer.css'
})
export class Viewer {
  @Element() private element: HTMLElement
  @Prop() canvasWidth: number = 500
  @Prop() url: string = ''
  @State() currentPage: number = 1
  @State() totalPages: number
  @State() error: any = undefined
  private loadingTask = undefined

  constructor() {
    pdfjsLib.GlobalWorkerOptions.workerSrc = pdfjsWorker
    this.loadingTask = pdfjsLib.getDocument(this.url)
  }

  // Listen to the active page event emited in the toolbar component
  @Listen('activePageEvent')
  setActivePage(event: CustomEvent): void {
    this.currentPage = event.detail
  }

  renderPage(): void {
    this.loadingTask.promise
      .then(doc => {
        this.totalPages = doc.numPages
        doc.getPage(this.currentPage).then(page => {
          let viewport = page.getViewport({scale: 1})
          const desiredScale = this.canvasWidth / viewport.width
          viewport = page.getViewport({ scale: desiredScale, })
          const canvas: any = this.element.querySelector('#tt-pdf-widget')
          const context = canvas.getContext('2d')
          canvas.height = viewport.height
          canvas.width = this.canvasWidth || viewport.width
          const renderContext = {
            canvasContext: context,
            viewport: viewport
          }
          page.render(renderContext)
        })
      })
      .catch(e => {
        this.error = e
      })
  }

  componentDidLoad(): void {
    this.renderPage()
  }

  componentWillUpdate(): void {
    this.renderPage()
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  render() {
    if (!isValidUrl(this.url)) {
      return (<error-message message={`Error: invalid URL ${this.url}`}></error-message>)
    }
    if (this.error) {
      return (<error-message message={this.error.message}></error-message>)
    }
    return (
      <div class='pdf-viewer background'>
        <pdf-toolbar
          currentPage={this.currentPage}
          totalPages={this.totalPages}
          url={this.url}
        ></pdf-toolbar>
        <canvas id='tt-pdf-widget'></canvas>
      </div>
    )
  }
}
