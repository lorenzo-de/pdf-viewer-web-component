/* eslint-disable import/prefer-default-export */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Component, State, Prop, Event, EventEmitter, h } from '@stencil/core'

@Component({
  tag: 'pdf-toolbar',
  styleUrl: 'toolbar.css'
})
export class Toolbar {
  @Prop() url: string
  @Prop() totalPages: number
  @Prop() currentPage: number
  @State() activePage: number = this.currentPage
  @Event({eventName: 'activePageEvent'}) activePageEvent: EventEmitter

  previousPage(): void {
    if (this.activePage > 1) {
      this.activePage--
      this.activePageEvent.emit(this.activePage)
    }
  }

  nextPage(): void {
    if (this.activePage < this.totalPages) {
      this.activePage++
      this.activePageEvent.emit(this.activePage)
    }
  }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  render() {
    return (
      <div class="pdf-toolbar border">
        <span
          class="pdf-toolbar op06 w pa1 small dib font"
        >
          Page {this.activePage} of {this.totalPages}
        </span>
        <span
          class="pdf-toolbar op06 link w pa1 small dib font"
          onClick={(): void => this.previousPage()}
        >
          Previous
        </span>
        <span
          class="pdf-toolbar op06 link w pa1 small dib font"
          onClick={(): void => this.nextPage()}
        >
          Next
        </span>
        <span class="pdf-toolbar pa1 dib">
          <a class="pdf-toolbar op06 link w small no-underline font" href={this.url} target="_blank">Open file</a>
        </span>
      </div>
    )
  }
}