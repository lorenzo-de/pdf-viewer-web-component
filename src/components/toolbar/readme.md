# pdf-toolbar



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute      | Description | Type     | Default     |
| ------------- | -------------- | ----------- | -------- | ----------- |
| `currentPage` | `current-page` |             | `number` | `undefined` |
| `totalPages`  | `total-pages`  |             | `number` | `undefined` |
| `url`         | `url`          |             | `string` | `undefined` |


## Events

| Event             | Description | Type               |
| ----------------- | ----------- | ------------------ |
| `activePageEvent` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [pdf-viewer](../viewer)

### Graph
```mermaid
graph TD;
  pdf-viewer --> pdf-toolbar
  style pdf-toolbar fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
