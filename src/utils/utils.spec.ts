import { isValidUrl } from './utils'

describe('isValidUrl', () => {
  it('should return false if argument is not a valid URL', () => {
    expect(isValidUrl('example.com')).toBeFalsy()
  })

  it('should return true if argument is a valid URL', () => {
    expect(isValidUrl('https://example.com')).toBeTruthy()
  })
})
