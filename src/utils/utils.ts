/* eslint-disable import/prefer-default-export */

export const isValidUrl = (value: string): boolean => {
  try {
    new URL(value)
    return true
  } catch (TypeError) {
    return false
  }
}