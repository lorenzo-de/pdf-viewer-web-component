# PDF viewer widget

Embeddable widget which displays a browsable PDF preview using Web Components built with [Stencil](https://stenciljs.com/) and [PDF.js](https://mozilla.github.io/pdf.js/).


Check out the [Stencil docs](https://stenciljs.com/docs/introduction) for the various build types.

## Usage

### Vanilla HTML/JS

```
<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=5.0">
  <title>Widget</title>

  <script type="module" src="/build/pdf-viewer.esm.js"></script>
  <script nomodule src="/build/pdf-viewer.js"></script>

</head>
<body>

  <div style="width: 600px;">
    <pdf-viewer
      canvas-width="600"
      url="http:://example.org/file.pdf"
    >
    </pdf-viewer>
  </div>

</body>
</html>
```

### NPM package in React application

Component JSX:

```
  <div style="width: 600px;">
    <pdf-viewer
      canvas-width="600"
      url="http:://example.org/file.pdf"
    >
    </pdf-viewer>
  </div>
```

index.js:

```
import { applyPolyfills, defineCustomElements } from 'pdf-viewer-web-component/dist/loader'

render(
  <App />, document.getElementById('root')
)

applyPolyfills().then(() => {
  defineCustomElements(window)
})

```

## Install

Tested with Node v12.13.0

```npm install```

## Development

Do the following steps in order embed a locally served PDF:

1. Download [this PDF](https://cdn.ttc.io/s/tacticaltech.org/Personal-Data-Political-Persuasion-How-it-works.pdf) into a folder
2. CD into the folder
3. ```npm install -g http-server```
4. Run command ```http-server --cors -p 8081``` to serve the example PDF. Make sure the PDF is served at http://localhost:8081/Personal-Data-Political-Persuasion-How-it-works.pdf
5. Open a new terminal and CD into this project directory
6. Run ```npm run dev``` to open a browser which displays the widget with the PDF.

## Production

To build the component for production, run:

```npm run build```

It is important to run ```npm run build``` before ```npm run publish``` otherwise not all files get included in the distribution.

## Tests

To run the unit and end to end tests for the components, run:

```npm run test```

## Troubleshooting

- Update to Webpack 4.41.2 in case you are using webpack and the npm package of this and get a `Can't import the named export 'a' from non EcmaScript module (only default export is available)` error.

