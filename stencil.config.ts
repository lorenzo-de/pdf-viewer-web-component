/* eslint-disable react/jsx-filename-extension */
import { Config } from '@stencil/core'
import nodePolyfills from 'rollup-plugin-node-polyfills'

// eslint-disable-next-line import/prefer-default-export
export const config: Config = {
  namespace: 'pdf-viewer',
  plugins: [
    nodePolyfills(),
  ],
  testing: {
    /**
     * Gitlab CI doesn't allow sandbox, therefor this parameters must be passed to your Headless Chrome
     * before it can run your tests.
     *
     * See also https://github.com/puppeteer/puppeteer/blob/master/docs/troubleshooting.md
     */
    browserArgs: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage'],
  },
  outputTargets: [
    {
      type: 'dist',
      // esmLoaderPath: '../loader'
    },
    {
      type: 'docs-readme'
    },
    {
      type: 'www',
      serviceWorker: null, // disable service workers
      copy: [
        { src: 'vendor/pdf.worker.js' } // Copy the Mozilla pdf.js script
      ]
    }
  ]
}
